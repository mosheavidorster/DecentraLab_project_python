from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Base:

    def __init__(self, driver):
        self.driver = driver
        self.default_duration = 50

    def wait_for_element_to_be_visible(self, element):
        self.wait_for_element_to_be_visible(element, self.default_duration)

    def wait_for_element_to_be_visible(self, element, duration):
        WebDriverWait(self.driver, duration).until(EC.visibility_of(element))

    def wait_for_element_to_be_clickable(self, element):
        self.wait_for_element_to_be_clickable(element, self.default_duration)

    def wait_for_element_to_be_clickable(self, element, duration):
        WebDriverWait(self.driver, duration).until(EC.element_to_be_clickable(element))
