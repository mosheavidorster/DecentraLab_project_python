import unittest

from cryptocurrency_transactions.infra.constants import ConstHordPage as CHP


class SideBarToggle:

    def __init__(self, driver, base):
        self.driver = driver
        self.base = base
        self.js_executor = self.driver.execute_script

        self.toggle = self.driver.find_element_by_css_selector(CHP.ConstHordPage.SIDEBAR_TOGGLE)
        self.hidden = self.driver.find_element_by_css_selector(CHP.ConstHordPage.HIDDEN_SIDEBAR_TOGGLE)
        self.aside = self.driver.find_element_by_xpath(CHP.ConstHordPage.ASIDE)

    def click_toggle(self):
        self.scroll_to_element(self.toggle)
        self.click_element(self.toggle)

    def click_hidden(self):
        self.scroll_to_element(self.hidden)
        self.click_element(self.hidden)

    def scroll_to_element(self, toggle):
        self.toggle = toggle
        self.base.wait_for_element_to_be_visible(toggle)
        self.js_executor(CHP.ConstHordPage.SIDEBAR_TOGGLE_JSEXECUTER, toggle)

    def click_element(self, hidden):
        self.hidden = hidden
        self.base.wait_for_element_to_be_clickable(hidden)
        self.js_executor(CHP.ConstHordPage.SIDEBAR_TOGGLE_JSEXECUTER_CLICK, hidden)

    def assert_sidebar_expanded(self, expected):
        actual_class = self.aside.get_attribute("class")

        if expected:
            unittest.TestCase().assertTrue(CHP.ConstHordPage.EXPENDED in actual_class,
                                           CHP.ConstHordPage.SIDE_BAR_EXPENDED_VERIFY_FAILED)
        else:
            unittest.TestCase().assertFalse(CHP.ConstHordPage.EXPENDED in actual_class,
                                            CHP.ConstHordPage.SIDE_BAR_EXPENDED_VERIFY)
