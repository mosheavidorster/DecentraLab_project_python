import unittest

from selenium.webdriver.common.by import By

from cryptocurrency_transactions.data.Base import Base

from cryptocurrency_transactions.infra.constants import ConstanTokensFarmPage as CTFP


class TokensFarmDropDown(Base):

    def __init__(self, driver):
        super().__init__(driver)

        self.pop_up_element = self.driver.find_element(By.XPATH, CTFP.ConstanTokensFarmPage.TOKEN_POPUP)
        self.token_farm_box = self.driver.find_element(By.CSS_SELECTOR, CTFP.ConstanTokensFarmPage.TOKEN_FARM_BOX_CSS)

    def get_pop_up_element(self):
        return self.pop_up_element

    def get_token_farm_box(self):
        return self.token_farm_box

    def click_pop_up_element(self):
        self.wait_for_element_to_be_visible(self.pop_up_element)
        unittest.TestCase().assertTrue(self.pop_up_element.is_displayed(),
                                       CTFP.ConstanTokensFarmPage.POPUP_DISPLAY_NOT_EXIST)
        self.pop_up_element.click()

    def click_token_farm_box(self):
        self.wait_for_element_to_be_clickable(self.token_farm_box)
        unittest.TestCase().assertTrue(self.token_farm_box.is_displayed(),
                                       CTFP.ConstanTokensFarmPage.TOKEN_FARM_DROPDOWN_NOT_EXIST)
        self.token_farm_box.click()
