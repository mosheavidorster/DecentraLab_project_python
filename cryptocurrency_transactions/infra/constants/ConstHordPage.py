class ConstHordPage:

    SIDEBAR_TOGGLE = ".sidebar-toggle-wrapper"
    HIDDEN_SIDEBAR_TOGGLE = ".sidebar-toggle-wrapper hovered"

    SIDEBAR_TOGGLE_JSEXECUTER = "arguments[0].scrollIntoView()"
    SIDEBAR_TOGGLE_JSEXECUTER_CLICK = "arguments[0].click()"

    SIDE_BAR_EXPENDED_VERIFY = "Assertion failed - Sidebar is expanded"
    SIDE_BAR_EXPENDED_VERIFY_FAILED = "Assertion failed - Sidebar is not expanded"
    EXPENDED = "expanded"
    ASIDE = "//aside[@class='duf-aside']"
