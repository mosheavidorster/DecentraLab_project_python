class ConstanTokensFarmPage:

    TOKEN_POPUP = "//span[@class='web3-wc_modal-icon-btn web3-wc_close-modal-btn']"

    TOKEN_FARM_BOX_CSS = "#farm-chain"
    TOKEN_FARM_BOX_SECOND_CSS = "#react-select-2-placeholder"
    TOKEN_FARM_BOX_XPATH = "//div[@id=\"react-select-2-placeholder\"]"
    POPUP_DISPLAY_NOT_EXIST = "Pop up element is not displayed"

    TOKEN_FARM_DROPDOWN_NOT_EXIST = "Token farm box is not displayed"
