
import allure
import pytest
from allure_commons.types import Severity
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


from cryptocurrency_transactions.actions.token_farm_action.TokensFarmDropDown import TokensFarmDropDown
from cryptocurrency_transactions.properties import TokensFarmLinks as TF

@pytest.fixture(scope="class")
def driver():
    chrome_options = Options()
    chrome_options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(options=chrome_options)
    driver.get(TF.TokensFarmLinks.TOKEN_FARM_URL)
    driver.maximize_window()
    yield driver


# Define a test class
@pytest.mark.usefixtures("driver")
class TestTokensFarm:

    @allure.title("Test the pop up element")
    @allure.description("This test verifies that the pop up element can be clicked")
    @allure.severity(Severity.NORMAL)
    def test_click_pop_up(self, driver):

        tokens_farm = TokensFarmDropDown(driver)

        tokens_farm.click_pop_up_element()

    @allure.title("Test the token farm box")
    @allure.description("This test verifies that the token farm box can be clicked")
    @allure.severity(Severity.NORMAL)
    def test_click_token_farm(self, driver):
        tokens_farm = TokensFarmDropDown(driver)

        tokens_farm.click_token_farm_box()


if __name__ == "__main__":
    pytest.main(["-v", "-s", "-ra"])
