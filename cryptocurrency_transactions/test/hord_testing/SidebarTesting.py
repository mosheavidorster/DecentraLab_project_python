import allure
import pytest
from allure_commons.types import Severity
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from cryptocurrency_transactions.actions.hord_action.SideBarToggle import SideBarToggle
from cryptocurrency_transactions.data.Base import Base
from cryptocurrency_transactions.properties import HordLinks as H


@pytest.fixture(scope="module")
def driver():
    chrome_options = Options()
    chrome_options.add_experimental_option("detach", True)
    driver = webdriver.Chrome(options=chrome_options)
    driver.get(H.HordLinks.HORD_URL)
    driver.maximize_window()
    yield driver


@pytest.mark.usefixtures("driver")
class TestSidebarTesting:

    @allure.title("Test the sidebar toggle functionality")
    @allure.description("This test verifies that the sidebar can be expanded and hidden by clicking the toggle button")
    @allure.severity(Severity.NORMAL)
    def test_toggle_expanded(self, driver):
        base = Base(driver)
        side_bar_toggle = SideBarToggle(driver, base)
        side_bar_toggle.click_toggle()
        side_bar_toggle.assert_sidebar_expanded(True)
        side_bar_toggle.click_hidden()
        side_bar_toggle.assert_sidebar_expanded(False)


if __name__ == "__main__":
    pytest.main(["-v", "-s", "-ra"])
